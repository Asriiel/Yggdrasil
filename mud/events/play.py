from .event import Event2, Event3

class PlayEvent(Event2):
	NAME = "play"
	def perform(self):
		if not self.object.has_prop("instrument"):
			self.fail()
			return self.inform("play.failed")
		self.inform("play")
		
